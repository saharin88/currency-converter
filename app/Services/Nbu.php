<?php

namespace App\Services;

class Nbu implements ServiceInterface
{

	public function getRates(): array
	{
		$rates   = [];
		$content = file_get_contents('http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');
		if (!empty($content))
		{
			$rates   = json_decode($content, true);
			$rates[] = [
				'cc'   => 'UAH',
				'rate' => 1
			];

		}

		return $rates;
	}

}
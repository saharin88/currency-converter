<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

$di = new DI\Container();
$di->set('App\Services\ServiceInterface', \DI\create('App\Services\Nbu'));

try
{
	$service = $di->get('App\Services\ServiceInterface');
}
catch (Exception $e)
{
	exit($e->getMessage());
}

$view = new Slim\Views\PhpRenderer(dirname(__DIR__) . '/app/templates/');

try
{
	/** @var Zend\Diactoros\Response $response */
	$response = $view->render(new Zend\Diactoros\Response(), 'index.phtml', ['rates' => $service->getRates()]);

	$emitter = new Zend\HttpHandlerRunner\Emitter\SapiEmitter();

	return $emitter->emit($response);

}
catch (Exception $e)
{
	exit($e->getMessage());
}

